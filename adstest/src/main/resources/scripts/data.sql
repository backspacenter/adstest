INSERT INTO `library`.`author`(`AUTHOR_ID`,`NAME`) VALUES(unhex(replace(uuid(), '-', '')), 'Arthur Koestler');
INSERT INTO `library`.`author`(`AUTHOR_ID`,`NAME`) VALUES(unhex(replace(uuid(), '-', '')), 'Jeff Ike');
INSERT INTO `library`.`author`(`AUTHOR_ID`,`NAME`) VALUES(unhex(replace(uuid(), '-', '')), 'Edna Marakasov');
INSERT INTO `library`.`author`(`AUTHOR_ID`,`NAME`) VALUES(unhex(replace(uuid(), '-', '')), 'Nathaniel Rense');
INSERT INTO `library`.`author`(`AUTHOR_ID`,`NAME`) VALUES(unhex(replace(uuid(), '-', '')), 'David Duke');
INSERT INTO `library`.`author`(`AUTHOR_ID`,`NAME`) VALUES(unhex(replace(uuid(), '-', '')), 'Andrew Carrington Hitchcock');

INSERT INTO `library`.`book`(`BOOK_ID`,`TITLE`,`ISBN_CODE`,`AUTHOR_ID`) VALUES (unhex(replace(uuid(), '-', '')), 'Challenge of Chance', '9780526041783', unhex('00397f1a78b011e89df9d017c2aad5e9'));
INSERT INTO `library`.`book`(`BOOK_ID`,`TITLE`,`ISBN_CODE`,`AUTHOR_ID`) VALUES (unhex(replace(uuid(), '-', '')), 'Asura Tale of The Vanquished', '9783528528818', unhex('00397f1a78b011e89df9d017c2aad5e9'));
INSERT INTO `library`.`book`(`BOOK_ID`,`TITLE`,`ISBN_CODE`,`AUTHOR_ID`) VALUES (unhex(replace(uuid(), '-', '')), 'Thirteenth Tribe', '9788504246201', unhex('0035EF7F78B011E89DF9D017C2AAD5E9'));
INSERT INTO `library`.`book`(`BOOK_ID`,`TITLE`,`ISBN_CODE`,`AUTHOR_ID`) VALUES (unhex(replace(uuid(), '-', '')), 'Ghost in the Machine', '9789312387072', unhex('0035EF7F78B011E89DF9D017C2AAD5E9'));
INSERT INTO `library`.`book`(`BOOK_ID`,`TITLE`,`ISBN_CODE`,`AUTHOR_ID`) VALUES (unhex(replace(uuid(), '-', '')), 'Lotus and the Robot', '9784195786677', unhex('0035EF7F78B011E89DF9D017C2AAD5E9'));
INSERT INTO `library`.`book`(`BOOK_ID`,`TITLE`,`ISBN_CODE`,`AUTHOR_ID`) VALUES (unhex(replace(uuid(), '-', '')), 'Scum of the Earth', '9782876296839', unhex('00397F1A78B011E89DF9D017C2AAD5E9'));