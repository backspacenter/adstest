package com.elouesse.adstest.repositories;

import com.elouesse.adstest.entities.Author;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for Author entity containing methods to access database
 * @author Elie
 */
@Repository
public interface AuthorRepository extends CrudRepository<Author, UUID> {
    
}
