package com.elouesse.adstest.repositories;

import com.elouesse.adstest.entities.Book;

import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for Book entity containing methods to access database
 * @author Elie
 */
@Repository
public interface BookRepository extends CrudRepository<Book, UUID> {
	
	/**
	 * SQL query to return a collection of all ISBN_Code column from Book table
	 * @return Set<String>
	 */
	@Query("SELECT bk.isbnCode FROM Book bk") 
	Set<String> findAllISBN();
    
}
