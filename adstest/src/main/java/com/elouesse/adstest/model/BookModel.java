package com.elouesse.adstest.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Model used to map values for the Book entity from the view to the controller
 *
 * @author Elie
 */
public class BookModel {
	@NotBlank
	@Size(min=1, max=120)
    private String title;
	@NotNull
    private String authId;

    public BookModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthId() {
    	
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }
    
}
