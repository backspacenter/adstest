package com.elouesse.adstest.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;


/**
 * The persistent class for the book database table.
 * 
 * @author Elie
 * 
 */
@Entity
@Table(name = "book")
public class Book implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BOOK_ID", columnDefinition="BINARY(16)")
	@GeneratedValue(generator="UUID-gen")
	@GenericGenerator(name="UUID-gen", strategy="uuid2")
	@Type(type = "org.hibernate.type.UUIDBinaryType")
	private UUID bookId;

	@NotNull
    @Size(min = 13, max = 13)
	@Column(name="ISBN_CODE")
	private String isbnCode;

	@NotNull
    @Size(min = 1, max = 120)
    @Column(name = "TITLE")
	private String title;

	//uni-directional many-to-one association to Author
	@ManyToOne
	@JoinColumn(name="AUTHOR_ID")
	private Author author;

	public Book() {
	}

	public UUID getBookId() {
		return this.bookId;
	}

	public void setBookId(UUID bookId) {
		this.bookId = bookId;
	}

	public String getIsbnCode() {
		return this.isbnCode;
	}

	public void setIsbnCode(String isbnCode) {
		this.isbnCode = isbnCode;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Author getAuthor() {
		return this.author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isbnCode == null) ? 0 : isbnCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (isbnCode == null) {
			if (other.isbnCode != null)
				return false;
		} else if (!isbnCode.equals(other.isbnCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", isbnCode=" + isbnCode + ", title=" + title + ", author=" + author.getName() + "]";
	}

}