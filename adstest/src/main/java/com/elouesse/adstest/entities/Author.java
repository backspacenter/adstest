package com.elouesse.adstest.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;


/**
 *  The persistent class for the author database table.
 * 
 * @author Elie
 * 
 */
@Entity
@Table(name = "author")
public class Author implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="AUTHOR_ID", columnDefinition="BINARY(16)")
	@GeneratedValue(generator="UUID-gen")
	@GenericGenerator(name="UUID-gen", strategy="uuid2")
	@Type(type = "org.hibernate.type.UUIDBinaryType")
	private UUID authorId;

	@NotNull
	@Column(name = "NAME")
	private String name;

	public Author() {
	}

	public UUID getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(UUID authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", name=" + name + "]";
	}

}