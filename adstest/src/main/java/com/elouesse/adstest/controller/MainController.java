package com.elouesse.adstest.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.elouesse.adstest.model.BookModel;
import com.elouesse.adstest.services.AuthorService;
import com.elouesse.adstest.services.BookService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * Contains the methods that control the UI interactions with the services
 * @author Elie
 */
@Controller
public class MainController {

	private final BookService bookService;
	private final AuthorService authorService;

	public MainController(BookService bookService, AuthorService authorService) {
		this.bookService = bookService;
		this.authorService = authorService;
	}

	@GetMapping("/")
	public String getBooks(Model model) {
		model.addAttribute("books", bookService.findAllBooks());
		return "index";
	}

	@GetMapping("/new")
	public String newBook(Model model) {
		model.addAttribute("book", new BookModel());
		model.addAttribute("authors", authorService.findAllAuthors());
		return "newbook";
	}

	@PostMapping("new")
	public String createBook(@Valid @ModelAttribute("book") BookModel bookModel, BindingResult bind, Model model) {
		if(bind.hasErrors()) {
			model.addAttribute("authors", authorService.findAllAuthors());
			return "newbook";
		}
		bookService.createBook(bookModel);
		return "redirect:/";
	}

	@GetMapping("/delete/{id}")
	public String deleteBook(@PathVariable("id") String id) {
		UUID uid = UUID.fromString(id);
		bookService.deleteBook(uid);
		return "redirect:/";
	}

	@GetMapping("/downloadlabel/{id}/{title}")
	public void downloadLabel(@PathVariable("id") String id, @PathVariable("title") String title,
			HttpServletResponse resp) throws SQLException, IOException, JRException {
			JasperPrint jasperPrint = bookService.exportPDF(id.replace("-", ""));
			resp.resetBuffer();
			resp.setContentType("application/pdf");
			resp.setHeader("Content-disposition", "inline; filename=");
			ServletOutputStream outputStream = resp.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView errorView(Exception e) {
		ModelAndView view = new ModelAndView("error");
		view.addObject("exception", e.getMessage());
		return view;
	}

}
