package com.elouesse.adstest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Class to start the application
 * @author Elie
 *
 */
@SpringBootApplication
public class AdstestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdstestApplication.class, args);
	}
}
