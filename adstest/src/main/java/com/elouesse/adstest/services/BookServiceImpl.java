package com.elouesse.adstest.services;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elouesse.adstest.entities.Author;
import com.elouesse.adstest.entities.Book;
import com.elouesse.adstest.model.BookModel;
import com.elouesse.adstest.repositories.BookRepository;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * Implementation of {@link BookService}
 * @author Elie
 */

@Service("bookService")
@Transactional
public class BookServiceImpl implements BookService {

	@Autowired
	private final BookRepository bookRepository;
	@Autowired
	private AuthorService authorService;

	private final String classpath = "src/main/resources/jasper/";

	private final String filename = "booklabel";

	@Value("${spring.datasource.url}")
	private String URL;
	@Value("${spring.datasource.username}")
	private String USER;
	@Value("${spring.datasource.password}")
	private String PWD;
	@Value("${spring.datasource.driver-class-name}")
	private String DRIVER;

	public BookServiceImpl(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@Override
	public void createBook(BookModel bookModel) {
		Book book = new Book();
		book.setTitle(bookModel.getTitle());
		if(book.getTitle().startsWith("#") || book.getTitle().startsWith("?"))
			book.setTitle(book.getTitle().substring(1,book.getTitle().length()));
		UUID uid = UUID.fromString(bookModel.getAuthId());
		Author author = authorService.findById(uid);
		book.setAuthor(author);
		book.setIsbnCode(generateISBNCode());
		bookRepository.save(book);
	}

	@Override
	public List<Book> findAllBooks() {
		List<Book> books = new ArrayList<Book>();
		bookRepository.findAll().iterator().forEachRemaining(books::add);
		return books;
	}

	@Override
	public Book findById(UUID id) {
		Book book = bookRepository.findById(id).get();
		return book;
	}

	@Override
	public void deleteBook(UUID id) {
		bookRepository.deleteById(id);
	}
	
	@Override
	public JasperPrint exportPDF(String id) throws JRException, SQLException {
		JasperReport jasperReport = null;
		InputStream jasperStream = this.getClass().getResourceAsStream(classpath + filename + ".jasper");
		if (jasperStream == null)
			jasperReport = buildJasperReport();
		else
			jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("ID_BOOK", id);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource().getConnection());
		return jasperPrint;
	}

	/**
	 * Generate random ISBN code
	 * @return String
	 */
	private String generateISBNCode() {
		String isbn = "";
		do {
			isbn = "978";
			int[] numbers = new int[9];
			int c = 58; // 978 checksum calculation

			for (int i = 0; i < numbers.length; i++) {
				numbers[i] = (int) (Math.random() * (10));
				if (numbers[i] == 10)
					numbers[i] = 0;
				isbn += numbers[i] + "";
				if (i % 2 == 0)
					c += numbers[i];
				else
					c += numbers[i] * 3;
			}
			c = c % 10;
			if (c == 10)
				c = 0;
			isbn += c + "";
		} while (!isISBNCodeUnique(isbn));
		return isbn;
	}

	/**
	 * Check if an ISBN code exists in database
	 * @param isbn
	 * @return boolean
	 */
	private boolean isISBNCodeUnique(String isbn) {
		Set<String> isbnList = bookRepository.findAllISBN();
		for (String s : isbnList)
			if (s.equals(isbn))
				return false;
		return true;
	}

	/**
	 * Get database connection for jasper report generation
	 * @return Datasource
	 */
	private DataSource dataSource() {
		return DataSourceBuilder.create().url(URL).username(USER).password(PWD).driverClassName(DRIVER).build();
	}

	/**
	 * Get classpath for jasper report template file and compile to a file
	 * @return JasperReport
	 * @throws JRException
	 */
	private JasperReport buildJasperReport() throws JRException {
		JasperCompileManager.compileReportToFile(classpath + "booklabel.jrxml", classpath + "booklabel.jasper");
		JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(classpath + filename + ".jasper");
		return jasperReport;
	}

}
