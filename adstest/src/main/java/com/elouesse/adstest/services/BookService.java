package com.elouesse.adstest.services;

import com.elouesse.adstest.entities.Book;
import com.elouesse.adstest.model.BookModel;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Interface containing the services related to the Book entity
 * @author Elie
 */
public interface BookService {
    
	/**
	 * Method to create a Book in the database.
	 * @param bookModel
	 */
    void createBook(BookModel bookModel);
    
    /**
     * Method to get all the Book records from the database.
     * @return List<Book>
     */
    List<Book> findAllBooks();
    
    /**
     * Method to get a single Book record from the database by specifying the UUID as parameter.
     * @param id
     * @return Book
     */
    Book findById(UUID id);
    
    /**
     * Method to delete a single Book record from the database by specifying the UUID as parameter.
     * @param id
     */
    void deleteBook(UUID id);
    
    /**
     * Method to create a JasperPrint object for generating a PDF export from the controller.
     * Takes a UUID as string parameter representing the Book entity UUID
     * @param id
     * @return JasperPrint
     * @throws JRException
     * @throws SQLException
     */
    JasperPrint exportPDF(String id) throws JRException, SQLException;
    
}
