package com.elouesse.adstest.services;

import com.elouesse.adstest.entities.Author;
import com.elouesse.adstest.repositories.AuthorRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link AuthorService}
 * @author Elie
 */
@Service("authorService")
@Transactional
public class AuthorServiceImpl implements AuthorService {
    
    @Autowired
    private final AuthorRepository authorRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> findAllAuthors() {
        List<Author> authors = new ArrayList<Author>();
        authorRepository.findAll().iterator().forEachRemaining(authors::add);
        return authors;
    } 

    @Override
    public Author findById(UUID id) {
        Optional<Author> author = authorRepository.findById(id);
        if(!author.isPresent())
        	throw new RuntimeException("Author not found");
        return author.get();
    }
    
}
