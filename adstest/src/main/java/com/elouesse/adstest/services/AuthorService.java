package com.elouesse.adstest.services;

import com.elouesse.adstest.entities.Author;
import java.util.List;
import java.util.UUID;

/**
 * Interface containing the services related to the Author entity
 * @author Elie
 */
public interface AuthorService {
    
	/**
     * Method to get all the Author records from the database.
     * @return List<Author>
     */
    List<Author> findAllAuthors();
    
    /**
     * Method to get a single Author record from the database by specifying the UUID as parameter.
     * @param id
     * @return Author
     */
    Author findById(UUID id);
    
}
