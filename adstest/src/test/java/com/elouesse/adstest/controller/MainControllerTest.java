package com.elouesse.adstest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.elouesse.adstest.services.AuthorService;
import com.elouesse.adstest.services.BookService;

/**
 * Unit tests for the controller of the application.
 * Designed to test the http responses.
 * 
 * @author Elie
 *
 */
public class MainControllerTest {
	
	MockMvc mockMvc;
	
	MainController mainCtrl;
	
	@Mock
	BookService bookService;
	
	@Mock
	AuthorService authorService;
	
	String testId = "ac2e492b-d2ba-42ce-a556-db811d8f243a";
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mainCtrl = new MainController(bookService, authorService);
		mockMvc = MockMvcBuilders.standaloneSetup(mainCtrl).build();
	}

	@Test
	public void testGetBooks() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(model().attributeExists("books")).andExpect(view().name("index"));
	}

	@Test
	public void testNewBook() throws Exception {
		mockMvc.perform(get("/new")).andExpect(status().isOk()).andExpect(model().attributeExists("book")).andExpect(model().attributeExists("authors"))
		.andExpect(view().name("newbook"));
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testCreateBook() throws Exception {
		mockMvc.perform(post("/new").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("title", "Unitest").param("authId", testId))
		.andExpect(status().isMovedTemporarily()).andExpect(redirectedUrl("/"));
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testDeleteBook() throws Exception {
		UUID deleteId = UUID.fromString(testId);
		mockMvc.perform(get("/delete/"+deleteId)).andExpect(status().isMovedTemporarily()).andExpect(redirectedUrl("/"));
	}

	@Test
	public void testDownloadLabel() throws Exception {
		UUID bookId = UUID.fromString(testId);
		String title = "testdummy";
		mockMvc.perform(get("/downloadlabel/"+bookId+"/"+title)).andExpect(status().isOk());
	}

}
