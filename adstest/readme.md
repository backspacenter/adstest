To configure the MySQL database look into application.properties and change the values accordingly.
SQL scripts to build the database are found in resources/scripts.
The application should start up by default using embedded tomcat on port 8080 from the spring-boot dependencies.